package br.up.edu.e1app07;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        EditText cn1 = (EditText) findViewById(R.id.txtNota1);
        EditText cp1 = (EditText) findViewById(R.id.txtPeso1);
        EditText cn2 = (EditText) findViewById(R.id.txtNota2);
        EditText cp2 = (EditText) findViewById(R.id.txtPeso2);
        EditText cn3 = (EditText) findViewById(R.id.txtNota3);
        EditText cp3 = (EditText) findViewById(R.id.txtPeso3);
        EditText cxMeida = (EditText) findViewById(R.id.txtMedia);

        String tn1 = cn1.getText().toString();
        String tp1 = cp1.getText().toString();
        String tn2 = cn2.getText().toString();
        String tp2 = cp2.getText().toString();
        String tn3 = cn3.getText().toString();
        String tp3 = cp3.getText().toString();

        double nota1 = Double.parseDouble(tn1);
        int peso1 = Integer.parseInt(tp1);
        double nota2 = Double.parseDouble(tn2);
        int peso2 = Integer.parseInt(tp2);
        double nota3 = Double.parseDouble(tn3);
        int peso3 = Integer.parseInt(tp3);

        double dividendo = nota1 * peso1 + nota2 * peso2 + nota3 * peso3;
        double divisor = peso1 + peso2 + peso3;
        double media =  dividendo / divisor;

        cxMeida.setText(String.valueOf(media));

    }

}







